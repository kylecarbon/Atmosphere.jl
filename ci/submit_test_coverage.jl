
include("init.jl")

cd(Pkg.dir("Atmosphere"))

Pkg.add("Coverage")
using Coverage
Codecov.submit_local(process_folder())
