#!/usr/bin/env julia
using Pkg

# Set MotionPlanning as primary environment for package manager
cur_dir = @__DIR__
project_dir = abspath(joinpath(cur_dir, "../"))
Pkg.activate(project_dir)
Pkg.instantiate()

import Atmosphere
const am = Atmosphere
