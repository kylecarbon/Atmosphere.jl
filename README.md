# Atmosphere.jl
[![pipeline status](https://gitlab.com/kylecarbon/Atmosphere.jl/badges/master/pipeline.svg)](https://gitlab.com/kylecarbon/Atmosphere.jl/pipelines)
[![coverage report](https://gitlab.com/kylecarbon/Atmosphere.jl/badges/master/coverage.svg)](https://codecov.io/gl/kylecarbon/Atmosphere.jl/branch/master)
[![documentation](https://img.shields.io/badge/docs-latest-blue.svg)](https://kylecarbon.gitlab.io/Atmosphere.jl/)

Simple package for calculating atmospheric parameters, e.g. pressure, temperature, density, etc.