using Test
import Atmosphere
const am = Atmosphere

@time @testset "Atmosphere.jl" begin include("atmosphere_tests.jl") end
@time @testset "Examples" begin include("examples_tests.jl") end
