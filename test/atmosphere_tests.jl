@testset "Temperature" begin
    @test am.temperature(0) ≈ am.T₀
    @test_throws DomainError am.temperature(-1000)

    ha = 0
    hb = 10
    ta = am.temperature(ha)
    tb = am.temperature(hb)
    slope = (tb - ta) / (hb - ha)
    @test slope ≈ -0.0065
    @test am.temperature(11000) ≈ 216.65
    @test am.temperature(20000) ≈ 216.65
    @test am.temperature(32000) ≈ 228.65
    @test am.temperature(47000) ≈ 270.65
    @test am.temperature(51000) ≈ 270.65
    @test am.temperature(71000) ≈ 214.65
    @test am.temperature(84852) ≈ 186.87
end

@testset "Pressure" begin
    @test am.pressure(0) ≈ am.P₀
    @test_throws DomainError am.pressure(-1000)
    @test am.pressure(11000) ≈ 22632.1
    @test am.pressure(20000) ≈ 5474.89
    @test am.pressure(32000) ≈ 868.019
    @test am.pressure(47000) ≈ 110.906
    @test am.pressure(51000) ≈ 66.9389
    @test am.pressure(71000) ≈ 3.95642
    @test am.pressure(84852) ≈ 0.3734
end

@testset "Density" begin
    P, T = am.isa(0)

    @test_throws DomainError am.density(P, T, -1)
    ρ = am.density(P, T, 0.0)
    @test ρ ≈ 1.225201448

    @test ρ > am.density(P, T, 1.0)
end

@testset "Gravity" begin
    @test am.gravity(0) ≈ am.g
end

@testset "Miscellaneous" begin
    am.greet()
    am.projectdir()
end
