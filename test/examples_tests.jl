# simply run these examples to make sure they run
ex_density = joinpath(am.projectdir(), "examples/plot_density.jl")
include(ex_density)

generate_literate = joinpath(am.projectdir(), "examples/generate_literate.jl")
include(generate_literate)
