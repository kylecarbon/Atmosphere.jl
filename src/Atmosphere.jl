module Atmosphere

export isa, pressure, temperature, density, gravity, water_vapour_pressure

"""
    greet()

Welcome the user to Atmosphere.jl
"""
greet() = println("Welcome to Atmosphere.jl!")

projectdir() = abspath(joinpath(@__DIR__, ".."))

P₀ = 101325.0 # [Pa] sea-level pressure
T₀ = 288.15 # [K] sea-level temperature
ρ₀ = 1.2250 # [kg / m³] sea-level density
g = 9.80665 # [m / s²] acceleration due to gravity at earth-surface
R = 8.314462618 # [J / mol / K] ideal universal gas constant according to 1976 US Standard Atmosphere model
Md = 0.02896968 # [ kg / mol ] molar mass of dry air
Mv = 0.01801528 # [ kg / mol ]
Rd = R / Md # [J / kg /K] specific gas constant for dry air
Rv = R / Mv # [J / kg /K] specific gas constant for water vapour

"""
    isa(h)

Return a tuple of (Pressure [Pa], Temperature [K]) given altitude in [meters]
above sea level based on the ISO International Standard Atmosphere.

* <https://en.wikipedia.org/wiki/International_Standard_Atmosphere>
* <https://en.wikipedia.org/wiki/Barometric_formula>
"""
function isa(h=0.0)
    # Columns are [altitude (m), standard temperature (K), temperature lapse rate, pressure (Pa)]
    table_lookup = [-610.0 292.115 -0.0065 108900.0;
                   0.0 288.15 -0.0065 101325.0;
                   11000.0 216.65 0.0 22632.1;
                   20000.0 216.65 0.001 5474.89;
                   32000.0 228.65 0.0028 868.019;
                   47000.0 270.65 0.0 110.906;
                   51000.0 270.65 -0.0028 66.9389;
                   71000.0 214.65 -0.002 3.95642;
                   84852.0 186.87 0 0.3734]

    lower_alt_limit = table_lookup[1,1]
    upper_alt_limit = table_lookup[end,1]

    if !(lower_alt_limit <= h <= upper_alt_limit)
        throw(DomainError(h, "No data exists according to the 1976 US Standard Atmosphere model for temperature lapse rate at h = $h meters."))
    end

    index = findfirst(x -> x > h, table_lookup[:, 1])
    if isnothing(index)
        return table_lookup[end, 4], table_lookup[end, 2]
    end
    index -= 1
    alt = table_lookup[index, 1]
    temp = table_lookup[index, 2]
    rate = table_lookup[index, 3]
    pressure = table_lookup[index, 4]

    T = temp + rate * (h - alt)
    if rate ≈ 0
        P = pressure * exp(-gravity(h) * Md * (h - alt) / R / temp)
    else
        P = pressure * (temp / T)^(gravity(h) * Md / R / rate)
    end
    return P, T
end

"""
    psat(T)

Saturation vapour pressure in [Pa] as a function of temperature [K] based on the
[Buck equation](https://en.wikipedia.org/wiki/Vapour_pressure_of_water)
"""
function psat(T)
    T = T - 273.15
    return T >= 0 ? 0.61121 * exp((18.678 - T / 234.5) * (T / (257.14 + T))) * 1000.0 :
                    0.61115 * exp((23.036 - T / 333.7) * (T / (279.82 + T))) * 1000.0
end

"""
    gravity(h)

Acceleration due to gravity as a function of altitude [m] above sea-level.
"""
function gravity(h)::Float64
    Re = 6.3781e6
    return g * (Re / (Re + h))^2
end

"""
    temperature(h)

Convenience function that returns the temperature [K] based on [`isa`](@ref).
"""
function temperature(h)::Float64
    _, T = isa(Float64(h))
    return T
end

"""
    pressure(h)

Convenience function that returns the pressure [Pa] based on [`isa`](@ref).
"""
function pressure(h=0.0)
    P, _ = isa(Float64(h))
    return P
end

"""
    water_vapour_pressure(P; T, ϕ)

Given total pressure [Pa], calculate dry air and water vapour pressure based on temperature [K] and
relative humidity ϕ [0, 1].
"""
function water_vapour_pressure(P=P₀; T=T₀, ϕ=0.0)::Tuple{Float64,Float64}
    Pv = ϕ * psat(T)
    Pd = P - Pv
    return Pd, Pv
end

"""
    enhancement_factor(P, T)

[CIPM-2007](https://www.nist.gov/system/files/documents/calibrations/CIPM-2007.pdf)
P: total pressure in [Pa]
T: temperature in [K]
"""
enhancement_factor(P, T) = 1.00062 + 3.14e-8 * P + 5.6e-7 * T^2

"""
    compressibility_factor(P, T, xv)

[CIPM-2007](https://www.nist.gov/system/files/documents/calibrations/CIPM-2007.pdf)
P: total pressure in [Pa]
T: temperature in [K]
xv: relative humidity [%]
"""
function compressibility_factor(P, T, xv)
    a₀ = 1.58123e-6
    a₁ = -2.9331e-8
    a₂ = 1.1043e-10
    b₀ = 5.707e-6
    b₁ = -2.051e-8
    c₀ = 1.9898e-4
    c₁ = -2.376e-6
    d = 1.83e-11
    e = -0.765e-8

    return 1.0 - P / T * (a₀ + a₁ * T + a₂ * T^2 +
                       (b₀ + b₁ * T) * xv +
                       (c₀ + c₁ * T) * xv^2
                       )
           + P^2 / T^2 * (d + e * xv^2)
end

"""
    density(P, T, ϕ=0.0)

Calculate density as a function of `P` [Pa], `T` [K], ϕ [%] based on
[CIPM-2007](https://www.nist.gov/system/files/documents/calibrations/CIPM-2007.pdf).

* `P` must be the total pressure (i.e. dry air and water vapour) and must be known a priori, e.g. through a model (such as the International Standard Atmosphere [`isa`](@ref Atmosphere.isa)) or via a measurement
* `T` is the system temperature in [K]
* ϕ is the relative humidity of the system that is bound by [0, 1]
"""
function density(P, T, ϕ=0.0)
    if !(0 <= ϕ <= 1)
        throw(DomainError(ϕ, "Relative humidity must be in the range [0, 1] -- ϕ = $ϕ is not valid."))
    end
    Pd, Pv = water_vapour_pressure(P, T=T, ϕ=ϕ)

    # Calculation according to Eq. 1 in CIPM-2007
    f = enhancement_factor(P, T)
    xv = f * Pv / P
    Z = iszero(ϕ) ? 1.0 : compressibility_factor(P, T, xv)
    ρ = P * Md / (Z * R * T) * (1.0 - xv * (1.0 - Mv / Md))

    # Alternate calculation from Engineering Toolbox
    # x = 0.62198 * Pv / Pd
    # ρ_alt = P / (Rd*T) * (1+x) / (1 + 1.609*x)
    # println("diff = $(ρ - ρ_alt)")
    return ρ
end

end # module
