# Atmosphere.jl
The [Atmosphere.jl](https://gitlab.com/kylecarbon/Atmosphere.jl) package provides simple
helper functions for working with Atmospheric models, e.g. the ISO International Standard
Atmosphere.

## Quickstart
```
using Pkg
Pkg.add("https://gitlab.com/kylecarbon/Atmosphere.jl")
using Atmosphere
```

## Tutorials
```@contents
Pages = ["tutorials/plot_density.md"]
Depth = 2
```

## APIs
```@contents
Pages = ["pages/atmosphere.md"]
Depth = 2
```
