using Documenter, Atmosphere

makedocs(format = Documenter.HTML(),
         sitename = "Atmosphere",
         pages = ["Home" => "index.md",
                  "Tutorials" => ["tutorials/plot_density.md"],
                  "API" => ["pages/atmosphere.md"],
                 ],
         repo = "https://gitlab.com/kylecarbon/Atmosphere.jl/blob/{commit}{path}#{line}"
         )
