using Literate

OUTPUT = joinpath(@__DIR__, "generated/")

poly_opt_example = joinpath(@__DIR__, "plot_density.jl")
Literate.markdown(poly_opt_example, OUTPUT; credit = false, documenter = true)
Literate.notebook(poly_opt_example, OUTPUT; credit = false)
