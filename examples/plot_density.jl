# # Calculate air density

import Atmosphere
const am = Atmosphere
using PlotlyJS

# This example will calculate density as a function of temperature and relative humidity at sea-level.
h = 0 # sea-level
temperatures = 0.0:5.0:100.0 # [0, 100] ᵒC -- this will have to be converted to Kelvin later
humidity = 0.0:.2:1.0

# Calculate pressure as a function of height at sea-level. Setting this constant, then vary temperature and relative humidity.
P = am.pressure(h)
density = zeros(length(temperatures), length(humidity))
for (j, T) ∈ enumerate(temperatures)
    for (k, ϕ) ∈ enumerate(humidity)
        density[j, k] = am.density(P, T + 273.15, ϕ)
    end
end

# Plot density vs. temperature.
traces = Vector{GenericTrace}()
for (index, ϕ) ∈ enumerate(humidity)
    ρ = density[:, index]
    trace = scatter(;x=temperatures,
                    y=ρ,
                    mode="lines",
                    name="RH: $(ϕ * 100) %")
    push!(traces, trace)
end
layout = Layout(;title="Air density vs. temperature with varying relative humidity (altitude: $h m)",
                 xaxis_title="Temperature (ᵒC)",
                 yaxis_title="Air density (kg / m³)"
               )
myplot = plot(traces, layout)
